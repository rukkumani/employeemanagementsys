import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  constructor(private fb: FormBuilder) { }

  createFullFrom() {
    return this.fb.group({
      _id: '',
      employeeRollNumber: ['', [Validators.required, Validators.maxLength(5)]],
      employeeName: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(25)]],
      employeeAge: [null, [Validators.required, Validators.maxLength(3)]],
      employeeMobileNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(12)]],
      employeeEmail: [{ value: '', disabled: false }, [Validators.required, Validators.email]],
      employeeAddress: this.fb.array([this.fb.group({
        _id: '',
        street: ["", Validators.required],
        city: ["", Validators.required],
        state: ["", Validators.required],
        zip: ["", Validators.required]
      }
      )]
      ),

    })
  }


}
