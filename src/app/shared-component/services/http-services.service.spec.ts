import { TestBed } from '@angular/core/testing';

import { HttpServices } from './http-services.service';

describe('HttpServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpServices = TestBed.get(HttpServices);
    expect(service).toBeTruthy();
  });
});
