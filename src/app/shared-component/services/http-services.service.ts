import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpServices {
  private API_URL = "https://employeemanagementsys.herokuapp.com/"
  getHeader() {
    return new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Accept', 'application/json; charset=utf-8')
  }

  constructor(private http: HttpClient) { }
  saveEmployeeDetaiils(employeeData): Observable<any> {
    console.log(`${this.API_URL}employeeDetails`)
    return this.http.post(`${this.API_URL}employeeDetails`, employeeData, {
      headers: this.getHeader(),
      observe: 'response'
    })
  }



  getAllTheEmployeeList() {
    return this.http.get(`${this.API_URL}employeeDetails`, {
      headers: this.getHeader(),
      observe: 'response'
    })
  }

  editEmployeeDetaiils(employeeData) {
    return this.http.put(`${this.API_URL}employeeDetails`, employeeData, {
      headers: this.getHeader(),
      observe: 'response'
    })
  }

  deleteEmployeeDetails(employeeId) {
    return this.http.delete(`${this.API_URL}employeeDetails/${employeeId}`, {
      headers: this.getHeader(),
      observe: 'response'
    })
  }
}
