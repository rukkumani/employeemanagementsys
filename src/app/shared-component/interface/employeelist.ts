import { FormArray,FormControl } from '@angular/forms';

export interface Employeelist{
    employeeRollNumber:string,
    employeeName:string,
    employeeAge:number,
    employeeMobileNumber:string,
    employeeEmail:string,
    employeeAddress:FormArray
}


export interface Address {
    street:string,
    city:string,
    state:string,
    zip:string
}
