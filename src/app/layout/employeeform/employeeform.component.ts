import { Component, OnInit, Input, OnChanges, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpServices } from "../../shared-component/services/http-services.service";
import { EmployeeService } from '../../shared-component/services/employee-service.service'
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-employeeform',
  templateUrl: './employeeform.component.html',
  styleUrls: ['./employeeform.component.css']
})
export class EmployeeformComponent implements OnInit, OnChanges {
  @Input() employeeForm: FormGroup
  @Output() submitEmployeeDetails: EventEmitter<object> = new EventEmitter()
  edit: boolean = false

  constructor(private httpService: HttpServices,
    private toaster: ToastrService,
    private employeeService: EmployeeService) { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    document.getElementById("employeeModal").click()
    this.employeeForm.patchValue(changes.employeeForm.currentValue)
    console.log(this.employeeForm.get("_id"))
    if (this.employeeForm.value._id) {
      this.edit = true
    } else {
      this.edit = false
    }
  }

  onSubmit(): void {
    try {
      if (this.employeeForm.value && Object.keys(this.employeeForm.value).length && !this.employeeForm.value._id) {
        if (this.employeeForm.value.employeeAddress) {
          delete (this.employeeForm.value._id)
          delete (this.employeeForm.value.employeeAddress[0]._id)
        }
        this.httpService.saveEmployeeDetaiils(this.employeeForm.value).subscribe(
          response => {
            if (response.status === 200 && response.body && Object.keys(response.body)) {
              this.toaster.success("Employee details added successfully")
              console.log(response.body)
              this.submitEmployeeDetails.emit(response.body)

            }
            document.getElementById("close").click()
          },
          err => {
            console.log(err)
            this.toaster.error(err.error.error ||
              'Error in saving Details.please check the value');
          })
      } else if (this.employeeForm.value && Object.keys(this.employeeForm.value).length && this.employeeForm.value._id) {
        this.httpService.editEmployeeDetaiils(this.employeeForm.value).subscribe(
          response => {
            if (response.status === 200 && response.body && Object.keys(response.body)) {
              this.toaster.success("Employee details added successfully")
              console.log(response.body)
              this.submitEmployeeDetails.emit(response.body)
            }
            document.getElementById("close").click()
          },
          err => {
            this.toaster.error(err.error.error ||
              'Error in saving Subline item. Try later');
          })

      } else {
        this.toaster.warning("Please fill the values")
      }

    } catch (e) {
      this.toaster.warning("Please try later")
    }

  }



  clearModal() {
    this.employeeForm = this.employeeService.createFullFrom()

  }

}
