import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, By } from '@angular/platform-browser';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule,ToastContainerModule  } from 'ngx-toastr';

import { DebugElement } from '@angular/core';
import { EmployeeformComponent } from './employeeform.component';
import { HttpServices } from "../../shared-component/services/http-services.service";
import { EmployeeService } from '../../shared-component/services/employee-service.service'
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';


describe('EmployeeformComponent', () => {
  let component: EmployeeformComponent;
  let fixture: ComponentFixture<EmployeeformComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeformComponent ],
      imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        ToastrModule.forRoot(),
        ToastContainerModule
      ],
      providers:[HttpServices,EmployeeService,ToastrService,{ provide: FormBuilder, useValue: formBuilder }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeformComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('form'));
    el = de.nativeElement;
    component.employeeForm = formBuilder.group({
        _id: '',
        employeeRollNumber: ['', [Validators.required, Validators.maxLength(5)]],
        employeeName: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(25)]],
        employeeAge: [null, [Validators.required, Validators.maxLength(3)]],
        employeeMobileNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(12)]],
        employeeEmail: [{ value: '', disabled: false }, [Validators.required, Validators.email]],
        employeeAddress: formBuilder.array([formBuilder.group({
          _id: '',
          street: ["", Validators.required],
          city: ["", Validators.required],
          state: ["", Validators.required],
          zip: ["", Validators.required]
        }
        )]
        ),
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should call the onSubmit method`, async(() => {
    spyOn(component, 'onSubmit');
    el = fixture.debugElement.query(By.css('.submit')).nativeElement;
    el.click();
    expect(component.onSubmit).toHaveBeenCalled();
  }));
  it(`form should be valid`, async(() => {
    component.employeeForm.setValue({
      _id:"222222222",
      employeeRollNumber: '12345',
      employeeName:"rukkumani",
      employeeAge: 12,
      employeeMobileNumber:"1234543234",
      employeeEmail:"rukkumani@gmail.com",
      employeeAddress: [{
        _id: 'ddd22222222',
        street:"ggggggggggg",
        city: "ddddddddddd",
        state: "ddddddddd",
        zip: "hhhhhh"
      }
      ]
  })

    expect(component.employeeForm.valid).toBeTruthy();
  }));
});
