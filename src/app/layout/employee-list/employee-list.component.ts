import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import { Employeelist, Address } from '../../shared-component/interface/employeelist';
import { HttpServices } from "../../shared-component/services/http-services.service";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employeeList
  employeeFormKey: string[]
  employeeForm: FormGroup
  constructor(private fb: FormBuilder,
    private httpService: HttpServices,
    private toaster: ToastrService
  ) { }

  ngOnInit() {
    this.getEmployeeList()
    this.createFullFrom()
  }


  getEmployeeList() {
    try {
      this.httpService.getAllTheEmployeeList().subscribe(
        respose => {
          if (respose && respose.status == 200 && respose.body && Array.isArray(respose.body) && respose.body.length) {
            this.employeeList = respose.body
            console.log("dddddddddddddddddd", this.employeeList)

          } else {
            this.employeeList = []
          }
        },
        err => {
          this.toaster.error("There is a problem in getting Employee list")
          this.employeeList = []
        }
      )
    } catch (e) {
      this.toaster.error("please try later")
    }

  }



  private createFullFrom() {
    this.employeeForm = this.createForm({
      _id: '',
      employeeRollNumber: ['', [Validators.required, Validators.maxLength(5)]],
      employeeName: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(25)]],
      employeeAge: [null, [Validators.required, Validators.maxLength(3)]],
      employeeMobileNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(12)]],
      employeeEmail: [{ value: '', disabled: false }, [Validators.required, Validators.email]],
      employeeAddress: this.fb.array([this.createForm({
        _id: '',
        street: ["", Validators.required],
        city: ["", Validators.required],
        state: ["", Validators.required],
        zip: ["", Validators.required]
      }
      )]
      ),

    })
  }







  private createForm(model): FormGroup {
    return this.fb.group(model);
  }

  private createAddressForm(model): FormArray {
    const address = new FormArray([]);

    model.forEach(element => {
      address.push(this.fb.group(element))
    });
    return address
  }

  getEmployeeeForm(data) {
    console.log("get employee from form ")
    console.log(data)
    this.getEmployeeList()
  }

  editEmployeeDetails(data) {
    this.employeeForm = this.fb.group({
      _id: data._id,
      employeeRollNumber: data.employeeRollNumber,
      employeeName: data.employeeName,
      employeeAge: data.employeeAge,
      employeeMobileNumber: data.employeeMobileNumber,
      employeeEmail: data.employeeEmail,
      employeeAddress: this.createAddressForm(data.employeeAddress),
    })
  }

  deleteEmployeeDetails(id, index) {
    try {
      this.httpService.deleteEmployeeDetails(id).subscribe(
        respose => {
          if (respose && respose.status == 200 && respose.body) {
            this.employeeList.splice(index, 1);
            this.toaster.success("Deleted Successfully")
          }
        },
        err => {
          this.toaster.error("There is a problem in getting Employee list")
        }
      )
    } catch (e) {
      this.toaster.warning("Please try later")
    }
  }


}
