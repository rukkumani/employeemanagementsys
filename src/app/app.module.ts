import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { AngularFontAwesomeModule } from 'angular-font-awesome';


import { EmployeeListComponent } from './layout/employee-list/employee-list.component';
import { EmployeeformComponent } from './layout/employeeform/employeeform.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule,ToastContainerModule  } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    EmployeeformComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ToastContainerModule,
    FontAwesomeModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
